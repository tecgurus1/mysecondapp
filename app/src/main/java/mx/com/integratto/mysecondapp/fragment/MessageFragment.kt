package mx.com.integratto.mysecondapp.fragment


import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import mx.com.integratto.mysecondapp.R
import mx.com.integratto.mysecondapp.dialog.TeatStyle

/**
 * A simple [Fragment] subclass.
 */
class MessageFragment : Fragment() {

    lateinit var btn: Button
    var algo: String? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.btnSnackbar).setOnClickListener {
            Snackbar.make(it, "Snack bar normal", Snackbar.LENGTH_LONG).show()
        }

        val btnSnackBarAction = view.findViewById<Button>(R.id.btnSnackbarAction)
        btnSnackBarAction.setOnClickListener {
            /*activity?.resources?.getColor(R.color.blue)?.let { it1 ->
                Snackbar.make(it, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setActionTextColor(ColorStateList.valueOf(it1))
                    .setActionTextColor(it1)
                    .setAction("Acción", null).show()
            }*/
            Snackbar.make(it, "Snack bar con accion", Snackbar.LENGTH_LONG)
                .setActionTextColor(ContextCompat.getColorStateList(it.context, R.color.blue))
                //.setActionTextColor(ContextCompat.getColor(it.context, R.color.blue))
                .setAction("Acción") {
                    Log.i("test", "Se preciona la acción")
                }.show()
        }

        val btnToast: Button = view.findViewById(R.id.btnToast)
        btnToast.setOnClickListener {
            Toast.makeText(it.context, "Esto es un Toast normal", Toast.LENGTH_LONG).show()
        }

        view.findViewById<Button>(R.id.btnCustomToast).setOnClickListener {
            activity?.let { TeatStyle.warningtoast(activity, layoutInflater, it, "Alerta") }
            val inflater = layoutInflater
            val container: ViewGroup? = activity?.findViewById(R.id.custom_toast_container)
            val layout: View = inflater.inflate(R.layout.custom_toast, container)
            val text: TextView = layout.findViewById(R.id.tvMessage)
            text.text = "Esto es un toast personalizado"

            val toast = Toast(activity?.applicationContext)
            toast.duration = Toast.LENGTH_SHORT
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
            toast.view = layout
            toast.show()
            /*with (Toast(activity?.applicationContext)) {
                setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                duration = Toast.LENGTH_LONG
                view = layout
                show()
            }*/
        }

        fun algo() {
            //btn
        }
    }
}
