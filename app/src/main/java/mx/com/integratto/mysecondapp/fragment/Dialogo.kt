package mx.com.integratto.mysecondapp.fragment


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast

import mx.com.integratto.mysecondapp.R
import mx.com.integratto.mysecondapp.dialog.DialogExample

/**
 * A simple [Fragment] subclass.
 */
class Dialogo : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.btnAlertDialog).setOnClickListener {
            AlertDialog.Builder(activity).setTitle("AlertDialog")
                .setMessage("Este es un alertdialog")
                .setCancelable(false)
                //.setNegativeButton("cancelar", null)
                .setNeutralButton("neutral") { _, _ ->
                    Toast.makeText(activity, "Neutral button", Toast.LENGTH_SHORT).show()
                }.setNegativeButton("negative") { _, _ ->
                    Toast.makeText(activity, "Negative button", Toast.LENGTH_SHORT).show()
                }.setPositiveButton("positive") { _, _ ->
                    Toast.makeText(activity, "Positive button", Toast.LENGTH_SHORT).show()
                }.create().show()
        }

        view.findViewById<Button>(R.id.btnDialog).setOnClickListener {
            activity?.let { DialogExample(it) }
        }
    }
}
