package mx.com.integratto.mysecondapp.model

import android.os.Parcel
import android.os.Parcelable

data class Persona(var id: Int,
                   var nombre: String,
                   var edad: Int,
                   var isHombre: Boolean,
                   var celular: Long,
                   var status: String): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readInt(),
        (parcel.readInt() == 1),
        parcel.readLong(),
        parcel.readString().toString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(nombre)
        parcel.writeInt(edad)
        parcel.writeInt(if (isHombre) 1 else 0)
        parcel.writeLong(celular)
        parcel.writeString(status)
    }

    override fun describeContents(): Int = 0


    companion object CREATOR : Parcelable.Creator<Persona> {
        override fun createFromParcel(parcel: Parcel): Persona {
            return Persona(parcel)
        }

        override fun newArray(size: Int): Array<Persona?> {
            return arrayOfNulls(size)
        }
    }

}