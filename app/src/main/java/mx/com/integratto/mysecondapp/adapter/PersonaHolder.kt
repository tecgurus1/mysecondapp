package mx.com.integratto.mysecondapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.com.integratto.mysecondapp.R
import mx.com.integratto.mysecondapp.model.Persona

class PersonaHolder(var personaList: List<Persona>): RecyclerView.Adapter<PersonaHolder.ViewHolder>() {

    companion object {
        lateinit var onItemSelectedClickListener: OnItemSelectedClickListener
        lateinit var personaList: List<Persona>
    }

    init {
        PersonaHolder.personaList = personaList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_persona, parent, false))

    override fun getItemCount(): Int = personaList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val persona = personaList[position]
        holder.tvName.text = persona.nombre
        holder.tvCellphone.text = "Cel. ${persona.celular}"
        holder.tvAge.text = "Edad ${persona.edad}"
        holder.tvGender.text = "sexo ${if (persona.isHombre) "Hombre" else "Mujer" }"
        holder.tvStatus.text = "Estatus ${persona.status}"
    }

    fun setOnItemSelectedClickListener(onItemSelectedClickListener: OnItemSelectedClickListener){
        PersonaHolder.onItemSelectedClickListener = onItemSelectedClickListener
    }

    interface OnItemSelectedClickListener { fun onItemSelected(persona: Persona) }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
        val tvCellphone = itemView.findViewById<TextView>(R.id.tvCellphone)
        val tvAge = itemView.findViewById<TextView>(R.id.tvAge)
        val tvGender = itemView.findViewById<TextView>(R.id.tvGender)
        val tvStatus = itemView.findViewById<TextView>(R.id.tvStatus)

        init {
            itemView.setOnClickListener {
                onItemSelectedClickListener.onItemSelected(personaList[adapterPosition])
            }
        }
    }
}