package mx.com.integratto.mysecondapp.dialog

import android.app.Activity
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import mx.com.integratto.mysecondapp.R

class TeatStyle {

    companion object {
        fun warningtoast(activity: Activity?, layoutInflater: LayoutInflater, context: Context, message: String) {
            val inflater = layoutInflater
            val container: ViewGroup? = activity?.findViewById(R.id.custom_toast_container)
            val layout: View = inflater.inflate(R.layout.custom_toast, container)
            val text: TextView = layout.findViewById(R.id.tvMessage)
            text.text = "Esto es un toast personalizado"

            val toast = Toast(activity?.applicationContext)
            toast.duration = Toast.LENGTH_SHORT
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
            toast.view = layout
            toast.show()
            /*with (Toast(activity?.applicationContext)) {
                setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                duration = Toast.LENGTH_LONG
                view = layout
                show()
            }*/
        }
    }
}