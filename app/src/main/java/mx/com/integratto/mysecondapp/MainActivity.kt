package mx.com.integratto.mysecondapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import mx.com.integratto.mysecondapp.fragment.PersonListFragment
import mx.com.integratto.mysecondapp.model.Persona

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val persona = intent.extras?.getParcelable<Persona>("persona")
        persona?.let {
            findViewById<TextView>(R.id.tvName).text = it.nombre
            findViewById<TextView>(R.id.tvGender).text = if (it.isHombre) "Hombre" else "Mujer"
            findViewById<TextView>(R.id.tvStatus).text = it.status
            findViewById<TextView>(R.id.tvAge).text = it.edad.toString()
            findViewById<TextView>(R.id.tvCellphone).text = it.celular.toString()
        }
    }
}
