package mx.com.integratto.mysecondapp.ui.main

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import mx.com.integratto.mysecondapp.R
import mx.com.integratto.mysecondapp.fragment.*

private val TAB_TITLES = arrayOf(
    R.string.homeFragment,
    R.string.messageFragment,
    R.string.personaListFragment,
    R.string.dialogFragment
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        //return PlaceholderFragment.newInstance(position + 1)
        val bundle = Bundle()
        bundle.putString("algo", "algo")
        return when (position) {
            0 -> HomeFragment()
            1 -> MessageFragment()
            2 -> PersonListFragment()
            3 -> Dialogo()
            else -> HomeFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? =
        context.resources.getString(TAB_TITLES[position])

    override fun getCount(): Int = 4
}