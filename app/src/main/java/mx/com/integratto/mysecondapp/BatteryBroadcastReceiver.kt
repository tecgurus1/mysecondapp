package mx.com.integratto.mysecondapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

private const val CHANNEL_ID = "BATERRY_NOTIFICATION"

class BatteryBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(p0: Context?, p1: Intent?) {

        var manager: NotificationManager? = null
        p0?.let {
            createNotificationChannel(it)
            manager = it.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            p0?.registerReceiver(null, ifilter)
        }

        val status: Int? = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
        val isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL

        if (isCharging) {
            Toast.makeText(p0, "Esta cargando", Toast.LENGTH_LONG).show()
            p0?.let {
                val builder = NotificationCompat.Builder(it, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_baterry_alert)
                    .setContentTitle(it.resources.getString(R.string.app_name))
                    .setContentText("Tu dispositivo se encuentra cargando")
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                manager?.notify(1, builder.build())

                with(NotificationManagerCompat.from(it)) {
                    notify(2, builder.build())
                }
            }
        } else {
            Toast.makeText(p0, "Esta llena la bateria", Toast.LENGTH_LONG).show()
            p0?.let {

                val intent = Intent(it, Main3Activity::class.java).apply {
                     flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
                val pendingIntent: PendingIntent = PendingIntent.getActivity(it, 0, intent, 0)

                val builder = NotificationCompat.Builder(it, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_baterry)
                    .setContentTitle(it.resources.getString(R.string.app_name))
                    .setContentText("Tu dispositivo esta cargado")
                    .setStyle(NotificationCompat.BigTextStyle()
                        .bigText("La bateria de tu dispositivo se encuentra al maximo se recomienda desconectarlo para conservar la vida de tu bateria"))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)

                manager?.notify(1, builder.build())

                with(NotificationManagerCompat.from(it)) {
                    notify(2, builder.build())
                }
            }
        }

        //val chargePlug: Int? = batteryStatus?.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
        //val usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB
        //val acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC
    }

    private fun createNotificationChannel(ctx: Context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = ctx.resources.getString(R.string.channel_name)
            val descriptionText = ctx.resources.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

}