package mx.com.integratto.mysecondapp.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mx.com.integratto.mysecondapp.MainActivity
import mx.com.integratto.mysecondapp.adapter.PersonaHolder

import mx.com.integratto.mysecondapp.R
import mx.com.integratto.mysecondapp.model.Persona

/**
 * A simple [Fragment] subclass.
 */
class PersonListFragment : Fragment() {

    companion object {
        lateinit var person: Persona
    }
    private val personaList = listOf(
        Persona(0, "Andres", 23, true, 2323424, "Activo"),
        Persona(1, "Juana", 45, false, 43645646, "Activo"),
        Persona(2, "Sandra", 12, false, 4354366, "Inactivo"),
        Persona(3, "Armando", 37, true, 35646464, "Activo"),
        Persona(4, "Ramiro", 69, true, 435647474, "Inactivo")
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recycler: RecyclerView = view.findViewById(R.id.recycler)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(activity)
        val personaHolder =
            PersonaHolder(personaList)
        recycler.adapter = personaHolder

        personaHolder.setOnItemSelectedClickListener(object: PersonaHolder.OnItemSelectedClickListener{
            override fun onItemSelected(persona: Persona) {
                person = persona
                val intent = Intent(activity, MainActivity::class.java)
                intent.putExtra("persona", persona)
                startActivity(intent)
            }
        })
    }

}
