package mx.com.integratto.mysecondapp.dialog

import android.app.Dialog
import android.content.Context
import android.widget.Button
import mx.com.integratto.mysecondapp.R

class DialogExample(context: Context) : Dialog(context, R.style.Theme_Dialog_Translucent) {


   init {
       setContentView(R.layout.dialogo)
       setCancelable(false)

       findViewById<Button>(R.id.btn).setOnClickListener {
           dismiss()
       }

       show()
   }

    fun showdialog() {
        show()
    }
}